{
  config,
  lib,
  pkgs,
  ...
}:

{
  # Podman setup
  virtualisation = {
    containers.enable = true;
    podman = {
      enable = true;
      dockerCompat = true;
      extraPackages = with pkgs; [ zfs ];
    };

    oci-containers = {
      backend = "podman";
      containers = {
        # Openbooks configuration
        openbooks = {
          image = "evanbuss/openbooks@sha256:4fa9188885368c2303b7dc527d48b3159aaa7022010e29b3ed96842018793590";
          ports = [ "127.0.0.1:9000:80" ];
          cmd = [
            "--name"
            "bradar"
            "--searchbot"
            "searchook"
            "--persist"
            "--tls"
            "false"
          ];
        };
        # Prometheus MQTT integration
        mqtt2prometheus = {
          image = "hikhvar/mqtt2prometheus@sha256:8e166d36feaa5ddcad703eef3a2c5167a154d6eef306a40fe6509861580c0714";
          ports = [ "127.0.0.1:9641:9641" ];
          volumes = [ "/vault/mqtt2prometheus/config.yaml:/config.yaml" ];
        };
      };
    };
  };

  # Start services after ZFS mount
  systemd.services.podman-mqtt2prometheus.unitConfig.RequiresMountsFor = [ /vault/mqtt2prometheus ];
}
