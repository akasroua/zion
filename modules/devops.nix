{
  config,
  pkgs,
  lib,
  ...
}:
{
  # Set up Gitea with LFS support
  services.gitea = {
    enable = true;
    database = {
      type = "postgres";
      passwordFile = config.age.secrets.gitea.path;
    };
    repositoryRoot = "/vault/git";
    appName = "Gitea";
    lfs = {
      enable = true;
      contentDir = "${config.services.gitea.repositoryRoot}/data/lfs";
    };
    settings = {
      server = {
        DISABLE_SSH = true;
        DOMAIN = "git.coolneng.duckdns.org";
        ROOT_URL = "https://git.coolneng.duckdns.org";
      };
      service.DISABLE_REGISTRATION = true;
      ui.DEFAULT_THEME = "arc-green";
      session.COOKIE_SECURE = true;
      actions.ENABLED = true;
    };
  };

  # Start services after ZFS mount
  systemd.services.gitea.unitConfig.RequiresMountsFor = [ /vault/git ];
}
