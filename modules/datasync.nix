{
  config,
  pkgs,
  lib,
  ...
}:
{

  # Syncthing configuration
  services.syncthing = {
    enable = true;
    openDefaultPorts = true;
    guiAddress = "0.0.0.0:8384";
    dataDir = "/vault/syncthing";
    key = config.age.secrets.syncthing.path;
    settings = {
      extraOptions.options = {
        maxFolderConcurrency = 4;
        progressUpdateIntervalS = -1;
      };
      devices = {
        panacea.id = "VEGVHKF-P4FT3BD-4T3ML7J-65URQOU-3XKNMI5-6LGWSCI-BIQZOUE-RKQ6PQX";
        caravanserai.id = "XQAXYEU-FWLAFZM-GTZYDGH-AIRBEXI-4CZD365-JUBTHDA-GOXXOYV-E5LEYQE";
      };
      folders = {
        Documents = {
          id = "wusdj-bfjkr";
          type = "receiveonly";
          path = "/vault/syncthing/Documents";
          devices = [
            "panacea"
            "caravanserai"
          ];
        };

        Notes = {
          id = "kafhz-bfmzm";
          type = "receiveonly";
          path = "/vault/syncthing/Notes";
          devices = [
            "panacea"
            "caravanserai"
          ];
        };

        Music = {
          id = "2aqt7-vpprc";
          type = "receiveonly";
          path = "/vault/syncthing/Music";
          devices = [
            "panacea"
            "caravanserai"
          ];
        };

        Photos = {
          id = "mjibc-ustcg";
          type = "receiveonly";
          path = "/vault/syncthing/Photos";
          devices = [
            "panacea"
            "caravanserai"
          ];
        };

        Projects = {
          id = "cjhmu-avy9v";
          type = "receiveonly";
          path = "/vault/syncthing/Projects";
          devices = [ "panacea" ];
        };

        Phone = {
          id = "m2007j20cg_vc7r-photos";
          type = "receiveonly";
          path = "/vault/syncthing/Photos/Phone";
          devices = [
            "panacea"
            "caravanserai"
          ];
        };

        Files = {
          id = "tsk52-u6rbk";
          type = "receiveonly";
          path = "/vault/syncthing/Files";
          devices = [
            "panacea"
            "caravanserai"
          ];
        };

        Phone-screenshots = {
          id = "pp70r-pbr70";
          type = "receiveonly";
          path = "/vault/syncthing/Photos/Phone-screenshots";
          devices = [
            "panacea"
            "caravanserai"
          ];
        };

        Audio = {
          id = "tarrs-5mxck";
          type = "receiveonly";
          path = "/vault/syncthing/Audio";
          devices = [
            "panacea"
            "caravanserai"
          ];
        };
      };
    };
  };

  # Enable Radicale
  services.radicale = {
    enable = true;
    settings = {
      server.hosts = [ "127.0.0.1:5232" ];
      auth = {
        type = "htpasswd";
        htpasswd_filename = "/vault/radicale/users";
        htpasswd_encryption = "md5";
        delay = 1;
      };
      storage.filesystem_folder = "/vault/radicale/collections";
    };
  };

  # ZFS automatic snapshots
  services.zfs.autoSnapshot = {
    enable = true;
    frequent = 4;
    hourly = 24;
    daily = 7;
    weekly = 4;
    monthly = 12;
  };

  # Start services after ZFS mount
  systemd.services.syncthing.unitConfig.RequiresMountsFor = [ /vault/syncthing ];
  systemd.services.radicale.unitConfig.RequiresMountsFor = [ /vault/radicale ];

}
