{
  config,
  lib,
  pkgs,
  ...
}:

let
  stateDir = "/var/lib/dnscrypt-proxy";
  blocklist = "${stateDir}/blocklist.txt";

in
{
  # PostgreSQL daily backups
  services.postgresqlBackup = {
    enable = true;
    backupAll = true;
    location = "/vault/backups/zion/databases";
    startAt = "*-*-* 05:15:00";
  };

  # Fetch hosts-blocklists daily
  # TODO Download the list if the file doesn't exist the first time
  systemd.services.download-dns-blocklist = {
    description = "Download hosts-blocklists";
    wantedBy = [ "default.target" ];
    path = with pkgs; [
      curl
      coreutils
    ];
    script = ''
      curl -L https://download.dnscrypt.info/blacklists/domains/mybase.txt -o ${blocklist}
    '';
    serviceConfig.Type = "oneshot";
    startAt = "02:00:00";
  };

  # Push zion changes to git daily
  systemd.user.services.zion-push = {
    description = "Push zion changes to git";
    wantedBy = [ "default.target" ];
    path = with pkgs; [ git ];
    script = ''
      ${pkgs.git}/bin/git -C /home/coolneng/system pull
      ${pkgs.git}/bin/git -C /home/coolneng/system push
    '';
    serviceConfig.Type = "oneshot";
    startAt = "07:00:00";
    after = [ "network-online.target" ];
  };
}
