{
  config,
  lib,
  pkgs,
  ...
}:

{
  # Miniflux configuration
  services.miniflux = {
    enable = true;
    adminCredentialsFile = config.age.secrets.miniflux.path;
    config = {
      BASE_URL = "https://rss.coolneng.duckdns.org";
      DISABLE_HSTS = 1;
    };
  };

  # Php-fpm pool for Wallabag
  services.phpfpm.pools.wallabag = {
    user = "nginx";
    group = "nginx";
    settings = {
      "listen.owner" = config.services.nginx.user;
      "listen.group" = config.services.nginx.group;
      "listen.mode" = 600;
      "pm" = "ondemand";
      "pm.max_children " = 4;
      "pm.max_requests" = 32;
      "env[WALLABAG_DATA]" = config.environment.variables.WALLABAG_DATA;
    };
    phpEnv."PATH" = lib.makeBinPath [ pkgs.php ];
  };

  # Set environment variable pointing to wallabag configuration directory
  environment.variables.WALLABAG_DATA = "/var/lib/wallabag";

}
