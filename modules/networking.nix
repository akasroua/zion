{
  config,
  pkgs,
  lib,
  ...
}:

let
  wireguard_port = 1194;

in
{
  # Enable systemd-networkd
  networking = {
    hostName = "zion";
    hostId = "760bfad7";
    useDHCP = false;
    useNetworkd = true;
    dhcpcd.enable = false;
  };
  systemd.network.wait-online.enable = false;

  # Assign a static IP
  systemd.network.networks."24-home" = {
    name = "enp2s0";
    matchConfig.Name = "enp2s0";
    address = [ "192.168.129.2/23" ];
    gateway = [ "192.168.128.1" ];
    dns = [
      "1.1.1.1"
      "9.9.9.9"
    ];
    networkConfig.DNSSEC = "no";
  };

  # Dynamic DNS configuration
  services.ddclient = {
    enable = true;
    quiet = true;
    interval = "30min";
    protocol = "duckdns";
    domains = [ "coolneng.duckdns.org" ];
    passwordFile = config.age.secrets.ddclient.path;
  };

  # Firewall configuration
  networking.firewall = {
    allowedTCPPorts = [
      80 # HTTP
      443 # HTTPS
      53 # DNS
      8448 # Matrix
      1883 # MQTT
    ];
    allowedUDPPorts = [
      wireguard_port # Wireguard
      53 # DNS
    ];
  };

  # Wireguard setup
  systemd.network.netdevs."wg0" = {
    netdevConfig = {
      Kind = "wireguard";
      Name = "wg0";
    };
    wireguardConfig = {
      ListenPort = wireguard_port;
      PrivateKeyFile = config.age.secrets.wireguard.path;
    };
    wireguardPeers = [
      # panacea
      {
        wireguardPeerConfig = {
          PublicKey = "XMkTztU2Y8hw6Fu/2o4Gszij+EmNacvFMXuZyHS1n38=";
          AllowedIPs = [ "10.8.0.2/32" ];
        };
      }
      # caravanserai
      {
        wireguardPeerConfig = {
          PublicKey = "mCsTj09H7lfDDs8vMQkJOlItHtHQ6MPUyfGO5ZjBbVs=";
          AllowedIPs = [ "10.8.0.3/32" ];
        };
      }
    ];
  };

  systemd.network.networks."wg0" = {
    matchConfig.Name = "wg0";
    networkConfig = {
      Address = "10.8.0.1/24";
      IPv4Forwarding = true;
    };
  };

  # DNS server with ad-block
  services.dnsmasq = {
    enable = true;
    settings = {
      domain-needed = true;
      bogus-priv = true;
      no-resolv = true;

      listen-address = [
        "127.0.0.1"
        "192.168.129.2"
        "10.8.0.1"
      ];
      bind-interfaces = true;
      server = [ "127.0.0.1#43" ];

      cache-size = 10000;
      local-ttl = 300;

      conf-file = "${pkgs.dnsmasq}/share/dnsmasq/trust-anchors.conf";
      dnssec = false;

      address = "/coolneng.duckdns.org/192.168.129.2";
    };
  };

  # Encrypted DNS
  services.dnscrypt-proxy2 = {
    enable = true;
    upstreamDefaults = true;
    settings = {
      listen_addresses = [ "127.0.0.1:43" ];
      sources.public-resolvers = {
        urls = [ "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md" ];
        cache_file = "/var/lib/dnscrypt-proxy2/public-resolvers.md";
        minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
      };
      blocked_names.blocked_names_file = "/var/lib/dnscrypt-proxy/blocklist.txt";
    };
  };

}
