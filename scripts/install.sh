#!/bin/sh

partition_disk() {
    parted "$DISK" -- mklabel gpt
    parted "$DISK" -- mkpart ESP fat32 1MiB 1025MiB
    parted "$DISK" -- mkpart linux-swap 1025MiB 17409MiB
    parted "$DISK" -- mkpart primary 17409MiB 100%
    parted "$DISK" -- set 1 boot on
    mkfs.fat -F32 -n BOOT "$DISK"p1
    mkswap "$DISK"p2
    swapon "$DISK"p2
}

zfs_setup() {
    zpool import -f vault
    zpool create -f -o ashift=12 -o autotrim=on -O acltype=posixacl -O relatime=on \
        -O xattr=sa -O dnodesize=legacy -O normalization=formD -O mountpoint=none \
        -O canmount=off -O devices=off -R /mnt -O compression=zstd "$POOL_NAME" "$DISK"p3
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false "$POOL_NAME"/ephemeral
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false "$POOL_NAME"/ephemeral/nix
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false -o sync=disabled -o setuid=off "$POOL_NAME"/ephemeral/tmp
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false "$POOL_NAME"/stateful
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=true "$POOL_NAME"/stateful/home
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false "$POOL_NAME"/stateful/root
}

mount_datasets() {
    mount -t zfs sysion/stateful/root /mnt
    mkdir -p /mnt/boot
    mount "$DISK"p1 /mnt/boot
    mkdir -p /mnt/home/coolneng
    mount -t zfs sysion/stateful/home /mnt/home/coolneng
    mkdir -p /mnt/nix
    mount -t zfs sysion/ephemeral/nix /mnt/nix
    mkdir -p /mnt/tmp
    mount -t zfs sysion/ephemeral/tmp /mnt/tmp
}

install_system() {
    nixos-generate-config --root /mnt
    mv /mnt/etc/nixos/hardware-configuration.nix modules/hardware-configuration.nix
    nix-shell -p git --command "nixos-install --root /mnt --flake .#zion"
}

usage() {
    echo "Usage: install.sh <disk>"
    echo "disk: full path to the disk (e.g. /dev/sda)"
    exit 1
}

if [ $# != 1 ]; then
    usage
fi

DISK="$1"
POOL_NAME="sysion"

echo "Let's start by partitioning the disk"
partition_disk
echo "Starting up the ZFS machinery"
zfs_setup
echo "Mounting the horse"
mount_datasets
echo "Lift off to the NixOS planet"
install_system
echo "All ready, time to rejoice"
