#!/run/current-system/sw/bin/bash
# Kernel information
LINUX=$(uname -rs | cut -d " " -f2)

# System uptime
uptime=$(cut -f1 -d. </proc/uptime)
upDays=$((uptime / 60 / 60 / 24))
upHours=$((uptime / 60 / 60 % 24))
upMins=$((uptime / 60 % 60))
upSecs=$((uptime % 60))

# System load
MEMORY=$(free -m | awk 'NR==2{printf "%s/%sMB (%.2f%%)\n", $3,$2,$3*100/$2 }')
CPU_LOAD=$(uptime | cut -d: -f5)

echo "============================================================
 - Kernel..............: $LINUX
 - System load.........:$CPU_LOAD
 - Memory used.........: $MEMORY
 - System uptime.......: $upDays days $upHours hours $upMins minutes $upSecs seconds
============================================================"
services=(
	"syncthing.service"
	"radicale.service"
	"miniflux.service"
	"phpfpm-wallabag.service"
	"gitea.service"
	"dendrite.service"
	"matrix-as-telegram.service"
	"matrix-as-facebook.service"
	"matrix-as-signal.service"
	"signald.service"
	"nginx.service"
	"dnsmasq.service"
	"podman-openbooks.service"
	"mosquitto.service"
	"podman-mqtt2prometheus.service"
	"prometheus.service"
	"grafana.service"
)

for var in "${services[@]}"; do
	if [[ -z $var ]]; then
		printf "\n"
	else
		if systemctl -q is-active "${var}"; then
			printf "%-40s [\e[32mOK\e[39m]\n" "$var"
		else
			printf "%-40s [\e[31mFAIL\e[39m]\n" "$var"
		fi
	fi
done

echo "============================================================"
