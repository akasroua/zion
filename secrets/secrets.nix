let
  zion = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFRqINHR7/zc+c3/PuR+NeSsBHXXzBiEtFWSK6QaxQTW";
in
{
  "wireguard.age".publicKeys = [ zion ];
  "syncthing.age".publicKeys = [ zion ];
  "msmtp.age".publicKeys = [ zion ];
  "gitea.age".publicKeys = [ zion ];
  "ddclient.age".publicKeys = [ zion ];
  "miniflux.age".publicKeys = [ zion ];
  "git.age".publicKeys = [ zion ];
  "dendrite.age".publicKeys = [ zion ];
  "dendrite-postgres.age".publicKeys = [ zion ];
  "telegram.age".publicKeys = [ zion ];
  "mqtt-sender.age".publicKeys = [ zion ];
  "mqtt-receiver.age".publicKeys = [ zion ];
  "facebook.age".publicKeys = [ zion ];
  "signal.age".publicKeys = [ zion ];
  "acme.age".publicKeys = [ zion ];
}
