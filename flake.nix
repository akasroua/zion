{
  description = "System configuration for zion";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    nix-matrix-appservices = {
      url = "gitlab:coffeetables/nix-matrix-appservices";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      nixpkgs-unstable,
      agenix,
      nixos-hardware,
      nix-matrix-appservices,
      ...
    }@inputs:
    let
      system = "x86_64-linux";

      pkgs = import pkgs { inherit system; };

      pkgs-unstable = import inputs.nixpkgs-unstable { inherit system; };

      lib = nixpkgs.lib;

    in
    {
      nixosConfigurations.zion = lib.nixosSystem {
        inherit system;
        modules = [
          (import ./configuration.nix)
          agenix.nixosModules.age
          nix-matrix-appservices.nixosModule
          nixos-hardware.nixosModules.aoostar-r1-n100
        ];
        specialArgs = {
          inherit inputs;
          inherit pkgs-unstable;
        };
      };

    };
}
