DIR=$(HOME)/Projects/zion

switch:
	nixos-rebuild switch --fast --target-host root@zion \
	--build-host root@zion --flake path://$(DIR)#zion

.DEFAULT_GOAL := switch
